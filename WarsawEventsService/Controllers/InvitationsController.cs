﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;

namespace WarsawEventsService.Controllers
{
    public class InvitationsController : ApiController
    {


        IRepository<Invitation> invitationRepository;

        public InvitationsController(IRepository<Invitation> invitationRepository)
        {
            this.invitationRepository = invitationRepository;
        }

        [HttpPost]
        [UseSSL]
        public IHttpActionResult Post([FromBody]Invitation invitation)
        {
            //using (Repository<Invitation> invitationRepository = new Repository<Invitation>(new WarsawEventsContext()))
           // {
                Invitation old = invitationRepository.Get().Where(i => i.SendUserId == invitation.SendUserId && i.ReceiveUserId == invitation.ReceiveUserId).FirstOrDefault();
                if (old != null)
                    return BadRequest();//throw new HttpResponseException(HttpStatusCode.Forbidden);////return Request.CreateResponse(HttpStatusCode.Forbidden);
                invitationRepository.Insert(invitation);
                invitationRepository.Save();

                return Ok();
            //}
        }

        [HttpGet]
        [UseSSL]
        public IHttpActionResult AcceptInvitation(int SendId, int ReceiveId)
        {
            //using (Repository<Invitation> invitationRepository = new Repository<Invitation>(new WarsawEventsContext()))
           // {
                var invitationToEdit = invitationRepository.Get().
                    Where(i => i.ReceiveUserId == ReceiveId && i.SendUserId == SendId).FirstOrDefault();
                invitationToEdit.IsAccepted = true;
                invitationRepository.Update(invitationToEdit);
                invitationRepository.Save();//musi byc bo inaczej nie zapisuje zmian
                return Ok();
           // }
        }
    }
}
