﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;
using System.Diagnostics;
using System.Data.Entity;

namespace WarsawEventsService.Controllers
{
    public class PlacesController : ApiController
    {
        // GET: api/Places

        IRepository<Place> placeRepository;

        public PlacesController(IRepository<Place> placeRepository)
        {
            this.placeRepository = placeRepository;
        }

        //[HttpGet]
        [UseSSL]
        public IHttpActionResult Get()
        {
           // using (Repository<Place> placeRepository = new Repository<Place>(new WarsawEventsContext()))
            //{
                var entity = placeRepository.Get().ToList();
                if (entity == null)
                    return NotFound();
                return Ok( entity);
            //}
        }

        // [HttpGet]
        [UseSSL]
        public IHttpActionResult GetByName(string phrase)
        {
            //using (Repository<Place> placeRepository = new Repository<Place>(new WarsawEventsContext()))
            //{
                var entity = placeRepository.Get().Where((Place p) => p.PlaceName.Contains(phrase) || p.StreetName.Contains(phrase) ||
                    p.District.DistrictName.Contains(phrase)).
                    Include(p=>p.Grades).ToList();
                if (entity == null)
                    return NotFound();
                return Ok(entity);
            //}
        }

        // [HttpGet]
        [UseSSL]
        public IHttpActionResult GetById(int id)
        {
            //using (Repository<Place> placeRepository = new Repository<Place>(new WarsawEventsContext()))
            //{
                return Ok( placeRepository.Find(id));
            //}
        }

        //[HttpPost]
        [UseSSL]
        public IHttpActionResult Post([FromBody]Place place)
        {
            //using (Repository<Place> placeRepository = new Repository<Place>(new WarsawEventsContext()))
            //{
                placeRepository.Insert(place);
                placeRepository.Save();
                return Ok();
            //}
        }

        /*[HttpPut]
        public HttpResponseMessage Put(int id, [FromBody]Place p)//update sredniej
        {
            using (Repository<Place> placeRepository = new Repository<Place>(new WarsawEventsContext()))
            {
                var placeToEdit = placeRepository.Find(id);
                placeToEdit.Average = p.Average;
                placeRepository.Update(placeToEdit);
                placeRepository.Save();//musi byc bo inaczej nie zapisuje zmian
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }*/
    }
}
