﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;

namespace WarsawEventsService.Controllers
{
    public class UsersController : ApiController
    {

        private IRepository<User> userRepository;

        public UsersController(IRepository<User> userRepository )
        {
            this.userRepository = userRepository;
        }
        //[HttpGet]
       [UseSSL]
        public IHttpActionResult GetAll(string type = "Wszyscy", int idUsera = -1, string phrase="")
        {
            //using (Repository<User> userRepository = new Repository<User>(new WarsawEventsContext()))
            //{
                User user = userRepository.Get().Where(u => u.Id == idUsera).
                    Include((User u) => u.ReceiveInvitations).
                    Include((User u) => u.SendInvitations).FirstOrDefault();
                List<int> users = FilterUsersTool.GetUserList(type, user);
                IEnumerable<User> entity;

                if (users == null)
                {
                    entity = userRepository.Get().Where(u=>u.UserName.Contains(phrase)).
                    Include((User u) => u.ReceiveInvitations).
                    Include((User u) => u.SendInvitations).ToList();
                }
                else
                {
                    entity = userRepository.Get().Where(u => users.Contains(u.Id) && u.UserName.Contains(phrase)).
                    Include((User u) => u.ReceiveInvitations).
                    Include((User u) => u.SendInvitations).ToList();
                }
                
                if (entity == null)
                    return NotFound();
                return Ok( entity);
            //}
        }

        //[HttpGet]
        [UseSSL]
        public IHttpActionResult GetById(int id)
        {
            //using (Repository<User> userRepository = new Repository<User>(new WarsawEventsContext()))
            //{
                return Ok(userRepository.Find(id));
           // }
        }

        //  [HttpGet]
        [UseSSL]
        public IHttpActionResult GetByLogin(string login)
        {
            //using (Repository<User> userRepository = new Repository<User>(new WarsawEventsContext()))
            //{
                var entity = userRepository.Get().Where(u => u.UserName == login).
                    Include((User u) => u.ReceiveInvitations).
                    Include((User u) => u.SendInvitations).FirstOrDefault();
                if (entity == null)
                    return BadRequest();
                return Ok( entity);
           // }
        }

        // [HttpPost]
        [UseSSL]
        public IHttpActionResult Post([FromBody]User user)
        {
           // using (Repository<User> userRepository = new Repository<User>(new WarsawEventsContext()))
           // {
                if (userRepository.Get().Where(u => u.UserName == user.UserName).FirstOrDefault()!=null)
                    return NotFound();
                userRepository.Insert(user);
                userRepository.Save();
                User usr = userRepository.Get().Where(u => u.UserName == user.UserName).
                    Include((User u) => u.ReceiveInvitations).
                    Include((User u) => u.SendInvitations).FirstOrDefault();
                return Ok(usr);//potrzebuje jego id
            //}
        }

        /*[HttpPut]
        public HttpResponseMessage Put(int id, [FromBody]User user)//zmiana hasla
        {
            using (Repository<User> userRepository = new Repository<User>(new WarsawEventsContext()))
            {
                var entity = userRepository.Get(u => u.Id == id).FirstOrDefault();
                entity.Password = user.Password;
                userRepository.Update(entity);
                userRepository.Save();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }*/
    }
}
