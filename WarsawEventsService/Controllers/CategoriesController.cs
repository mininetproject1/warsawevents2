﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;

namespace WarsawEventsService.Controllers
{
    public class CategoriesController : ApiController
    {
        IRepository<Category> categoryRepository;

        public CategoriesController(IRepository<Category> categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }

        //[HttpGet]
        [UseSSL]
        public IHttpActionResult GetAll()
        {

                return Ok(categoryRepository.GetAll());

        }

        //[HttpGet]
        [UseSSL]
        public IHttpActionResult GetById(int id)
        {

                return Ok(categoryRepository.Find(id));

        }
    }
}
