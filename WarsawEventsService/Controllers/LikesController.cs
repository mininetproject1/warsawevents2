﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;

namespace WarsawEventsService.Controllers
{
    public class LikesController : ApiController
    {
      
        IRepository<Like> likeRepository;

        public LikesController(IRepository<Like> likeRepository)
        {
            this.likeRepository = likeRepository;
        }
       
        //[HttpPost]
        [UseSSL]
        public IHttpActionResult Post([FromBody]Like like)
        {
            //using (Repository<Like> likeRepository = new Repository<Like>(new WarsawEventsContext()))
           // {
                Like old = likeRepository.Get().Where(l => l.EventId == like.EventId && l.UserId == like.UserId).FirstOrDefault();
                if (old != null) //uzytkownik polajkowal
                    return BadRequest();
                likeRepository.Insert(like);
                likeRepository.Save();
                return Ok();
            //}
        }
    }
}
