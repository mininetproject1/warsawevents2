﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;

namespace WarsawEventsService.Controllers
{
    public class ParticipantController : ApiController
    {

        IRepository<Participant> participantRepository;
        IEventRepository eventsRepository;

        public ParticipantController(IRepository<Participant> participantRepository, IEventRepository eventsRepository)
        {
            this.participantRepository = participantRepository;
            this.eventsRepository = eventsRepository;
        }

        //[HttpPost]
        [UseSSL]
        public IHttpActionResult Post([FromBody]Participant part)
        {
            var context = new WarsawEventsContext();
            //using (Repository<Participant> participantRepository = new Repository<Participant>(context))
            //{
                Participant old = participantRepository.Get().Where(p => p.EventId == part.EventId && p.UserId == part.UserId).FirstOrDefault();
                if (old != null) //uzytkownik juz dolaczyl
                    return BadRequest();
                participantRepository.Insert(part);
                participantRepository.Save();
                IncrementParticipants(part.EventId,context);
                return Ok();
            //}
        }

        private void IncrementParticipants(int eventId, WarsawEventsContext context)
        {
            //using(var eventsRepository = new EventRepository(context))
            //{
                Event eventToUpdate = eventsRepository.Find(eventId);
                eventToUpdate.NumberOfParticipants++;
                eventsRepository.UpdateEvent(eventToUpdate);
                eventsRepository.Save();
            //}
        }
    }
}
