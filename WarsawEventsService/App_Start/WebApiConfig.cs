﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Microsoft.Practices.Unity;
using WarsawEvents.DataModel.Entities;
using WarsawEvents.DataModel;
using WarsawEventsService.Controllers;
using WarsawEventsService.App_Start;

namespace WarsawEventsService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            //DependencyInjection
            var container = new UnityContainer();
            container.RegisterType<IRepository<User>, Repository<User>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Place>, Repository<Place>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Participant>, Repository<Participant>>(new HierarchicalLifetimeManager());
            container.RegisterType<IEventRepository, EventRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Like>, Repository<Like>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Invitation>, Repository<Invitation>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Grade>, Repository<Grade>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<District>, Repository<District>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Comment>, Repository<Comment>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepository<Category>, Repository<Category>>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
