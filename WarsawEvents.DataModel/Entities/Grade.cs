﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarsawEvents.DataModel.Entities
{
    public class Grade 
    {
        [Key, Column(Order = 0)]
        public int PlaceId { get; set; }
        [Key, Column(Order = 1)]
        public int UserId { get; set; }
        [Range(1, 5)]
        public int Value { get; set; }

        //public virtual Place Place { get; set; }//były zapetlenia
        //public virtual User User { get; set; }
    }
}
