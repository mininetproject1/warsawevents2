﻿using System;
using System.Collections.Generic;

namespace WarsawEvents.DataModel.Entities
{
    public class Event: Entity
    {
        public string EventName { get; set; }
        public string Description { get; set; }
        public double Price { get; set; } 
        public DateTime DateOfStart { get; set; }
        public int NumberOfParticipants { get; set; }
        //Foreign keys
        public int PlaceId { get; set; }
        public int CategoryId { get; set; }

        public virtual Place Place { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Like> Likes { get; set; }
        public virtual ICollection<Participant> Participants { get; set; }

    }
}
