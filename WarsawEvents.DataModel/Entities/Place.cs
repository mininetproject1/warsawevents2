﻿using System.Collections.Generic;

namespace WarsawEvents.DataModel.Entities
{
    public class Place: Entity
    {
        public string PlaceName { get; set; }
        public string StreetName { get; set; }
        public int Streetnumber { get; set; }
        public double Average { get; set; }

        //Foreign key for District
        public int DistrictId { get; set; }
        public virtual District District { get; set; }
        public virtual ICollection<Grade> Grades { get; set; }
       // public virtual ICollection<Event> Events { get; set; }
    }
}
