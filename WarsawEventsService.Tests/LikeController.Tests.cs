﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;
using WarsawEventsService.Controllers;

namespace WarsawEventsService.Tests
{
    [TestFixture]
    class LikeControllerTests
    {
        [Test]
        public void PostLike()
        {
            IRepository<Like> rep = new MockRepository<Like>();
            Like like = new Like() { EventId = 0, UserId = 3 };
   

            var likesController = new LikesController(rep);
            likesController.Post(like);


            List<Like> likes = rep.GetAll().ToList();

            Assert.AreEqual(true, likes.Any((Like l) => l.EventId == like.EventId && l.UserId == like.UserId));
        }
    }
}
