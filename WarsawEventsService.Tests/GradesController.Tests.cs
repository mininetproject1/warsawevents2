﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;
using WarsawEventsService.Controllers;

namespace WarsawEventsService.Tests
{
    [TestFixture]
    class GradesControllerTests
    {
        [Test]
        public void PostGrade()
        {
            IRepository<Grade> rep = new MockRepository<Grade>();
            Grade grade = new Grade() { PlaceId = 0, UserId = 1 };
            IRepository<Place> repPlace = new MockRepository<Place>();
            repPlace.Insert(new Place() { Id = 0, Grades = new List<Grade>() });

            var gradesController = new GradesController(rep, repPlace);
            gradesController.Post(grade);


            List<Grade> grades = rep.GetAll().ToList();

            Assert.AreEqual(true, grades.Any((Grade g) => g.PlaceId == grade.PlaceId && g.UserId == grade.UserId));
        }
    }
}
