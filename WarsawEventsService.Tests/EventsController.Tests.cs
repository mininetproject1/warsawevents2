﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;
using WarsawEventsService.Controllers;

namespace WarsawEventsService.Tests
{
    [TestFixture]
    public class EventsControllerTests
    {

        [Test]
        public void AddEvent()
        {
            Event e1 = new Event { Id=0, EventName= "Test", DateOfStart = DateTime.Now.AddDays(1) };
            IEventRepository rep = new MockEventRepository();
            var eventsController = new EventsController(rep, new MockRepository<Place>(), 
                new MockRepository<Comment>(), new MockRepository<User>());
            eventsController.Post( e1);
            Event checkEvent = rep.GetCurrentEvents().First();
            Assert.AreEqual(true, checkEvent.Id == e1.Id && checkEvent.EventName == e1.EventName);
        }

        [Test]
        public void RemoveEvent()
        {
            Event e1 = new Event { DateOfStart = DateTime.Now.AddDays(1) };
            IEventRepository rep = new MockEventRepository();
            rep.InsertEvent(e1);
            var eventsController = new EventsController(rep, new MockRepository<Place>(), 
                new MockRepository<Comment>(), new MockRepository<User>());
            eventsController.Delete(e1.Id);
            int count = rep.GetCurrentEvents().Count();
            Assert.AreEqual(0, count);
        }



        [Test]
        public void FilterTwoEventsOneIsOk()
        {
            Category c1 = new Category { CategoryName = "Test" };
            Category c2 = new Category { CategoryName = "Test" };
            District d1 = new District { DistrictName = "Test" };
            District d2 = new District { DistrictName = "Test" };
            Place p1 = new Place { District = d1, PlaceName = "Test", StreetName = "Test" };
            Place p2 = new Place { District = d2, PlaceName = "Test", StreetName = "Test" };
            DateTime dt1 = DateTime.Now.AddYears(1);
            DateTime dt2 = DateTime.Now.AddYears(1);

            Event e1 = new Event { Id = 0, EventName = "Test1", Description = "Test", Place = p1, DateOfStart = dt1, Category = c1 , Comments = new List<Comment>()};
            Event e2 = new Event { Id = 1, EventName = "Test2", Description = "Test", Place = p2, DateOfStart = dt2, Category = c2, Comments = new List<Comment>() };


            IEventRepository rep = new MockEventRepository();
            rep.InsertEvent(e1);
            rep.InsertEvent(e2);
            var eventsController = new EventsController(rep, new MockRepository<Place>(),
                new MockRepository<Comment>(), new MockRepository<User>());

            var actionResult = eventsController.GetFiltered("Test1");

            IEnumerable<Event> events = (actionResult as OkNegotiatedContentResult<IEnumerable<Event>>).Content;

             Assert.AreEqual(true, events.Any((Event e) => e.Id == e1.Id ));
             Assert.AreEqual(false, events.Any((Event e) => e.Id == e2.Id ));
        }

        [Test]
        public void SortEvents()
        {
            Category c1 = new Category { CategoryName = "Test" };
            Category c2 = new Category { CategoryName = "Test" };
            District d1 = new District { DistrictName = "Test" };
            District d2 = new District { DistrictName = "Test" };
            Place p1 = new Place { District = d1, PlaceName = "Test", StreetName = "Test" };
            Place p2 = new Place { District = d2, PlaceName = "Test", StreetName = "Test" };
            DateTime dt1 = DateTime.Now.AddYears(2);
            DateTime dt2 = DateTime.Now.AddYears(1);

            Event e1 = new Event { Id = 0, EventName = "Test1", Description = "Test", Place = p1, DateOfStart = dt1, Category = c1, Comments = new List<Comment>() };
            Event e2 = new Event { Id = 1, EventName = "Test2", Description = "Test", Place = p2, DateOfStart = dt2, Category = c2, Comments = new List<Comment>() };


            IEventRepository rep = new MockEventRepository();
            rep.InsertEvent(e1);
            rep.InsertEvent(e2);
            var eventsController = new EventsController(rep, new MockRepository<Place>(),
                new MockRepository<Comment>(), new MockRepository<User>());

            var actionResult = eventsController.GetFiltered("", "Wszystkie", "Wszystkie", "Wszystkie","Wszystkie", -1, "Dacie"); ;

            IEnumerable<Event> events = (actionResult as OkNegotiatedContentResult<IEnumerable<Event>>).Content;

            Assert.AreEqual(true, events.FirstOrDefault().Id == e2.Id);

        }
    }
}
