﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace WarsawEventsMVVM.Model
{
    public class MockEventRepository
    {
        ObservableCollection<Event> listEvent;

        public  MockEventRepository()
        {
            listEvent  = new ObservableCollection<Event>();
            for (int i = 0; i < 15; ++i)
            {
                listEvent.Add(new Event()
                {
                    Category = new Category() { Id = 1, CategoryName = "Nauka" },
                    Place = new Place()
                    {
                        Id = i,
                        District = new District() { Id = 1, DistrictName = "Śródmieście" },
                        DistrictId = 1,
                        PlaceName = "placeName",
                        StreetName = "streetName",
                        Streetnumber = i
                    },
                    EventName = "Warsztaty familijne",
                    Description = "Dlaczego statki pływają? Dlaczego słońce świeci?",
                    Price = 500 - i* 10,
                    NumberOfParticipants = i*100,
                    DateOfStart = new DateTime(2016, 12, 30 - i, 11, 15, 0)
                });
            }//for

        }

        public void DeleteEvent(int eventId)
        {
            listEvent.RemoveAt(eventId);
        }

        public ObservableCollection<Event> GetCurrentEvents()
        {
            return listEvent;
        }

       

        public ObservableCollection<Event> GetEvents(IList<Expression<Func<Event, bool>>> funcList)
        {
           
            IQueryable<Event> query = listEvent.AsQueryable();

            foreach (Expression<Func<Event, bool>> func in funcList)
                query = query.Where(func);

            try //tutaj podczas debugowanie wyskakiwał mi wyjątek, którego nie rozumiałem, choć filtrowanie działało ok
            {
                return new ObservableCollection<Event>(query.ToList());
            }catch (Exception e)
            {
                return new ObservableCollection<Event>(listEvent);
            }
            //return new ObservableCollection<Event>(listEvent);
        }

        public void InsertEvent(Event newEvent)
        {
            listEvent.Add(newEvent);
        }

        public void Save()
        {
        }

        public void UpdateEvent(Event updatedEvent)
        {

        }
    }
}
