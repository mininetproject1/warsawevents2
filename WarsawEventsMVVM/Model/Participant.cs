﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarsawEventsMVVM.Model
{
    public class Participant
    {
 
        public int EventId { get; set; }
        public int UserId { get; set; }
        public bool? IsOwner { get; set; }
        //public virtual Event Event { get; set; }
        public virtual User User { get; set; }
    }
}
