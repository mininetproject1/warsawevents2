﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarsawEventsMVVM.Model
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsLogged { get; set; }
        public virtual ICollection<Invitation> SendInvitations { get; set; }
        public virtual ICollection<Invitation> ReceiveInvitations { get; set; }
    }
}
