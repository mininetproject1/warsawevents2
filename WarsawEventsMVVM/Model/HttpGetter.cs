﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WarsawEventsMVVM.Model
{
    public class HttpGetter
    {
        private string uriPref;

        public HttpGetter()
        {
            uriPref = "https://localhost:44321/";//:44321
        }//HttpGetter

        //public IEnumerable<Event> GetEvents()
        //{
        //        HttpResponseMessage response = client.GetAsync("api/events?category=Nauka").Result;

        //        if (response.IsSuccessStatusCode)
        //        {
        //            return response.Content.ReadAsAsync<IEnumerable<Event>>().Result;

        //         }
        //        else
        //        {
        //            MessageBox.Show("Error Code" +
        //            response.StatusCode + " : Message - " + response.ReasonPhrase);
        //        }
        //        return null;

        //}//GetEvents

        public async Task<bool> PostParticipant(Participant p)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(uriPref);

            client.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.PostAsJsonAsync("api/participant", p);
            if (response.IsSuccessStatusCode)
            {
                // MessageBox.Show("User Added");
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> PostLike(Like like)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(uriPref);

            client.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.PostAsJsonAsync("api/likes", like);
            if (response.IsSuccessStatusCode)
            {
                // MessageBox.Show("User Added");
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> PostComment(Comment c)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(uriPref);

            client.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.PostAsJsonAsync("api/comments", c);
            if (response.IsSuccessStatusCode)
            {
                // MessageBox.Show("User Added");
                return true;
            }
            else
            {
                MessageBox.Show("Error Code" +
                response.StatusCode + " : Message - " + response.ReasonPhrase);
                return false;
            }

        }//Post

        public async Task<Place> PostGrade(Grade g)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(uriPref);

            client.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.PostAsJsonAsync("api/grades", g);
            if (response.IsSuccessStatusCode)
            {
                // MessageBox.Show("User Added");
                return await response.Content.ReadAsAsync<Place>();
            }
            else
            {
                //MessageBox.Show("Error Code" +
                //response.StatusCode + " : Message - " + response.ReasonPhrase);
                return null;
            }

        }//Post

        public async Task<bool> PutInvitation(int SendId, int ReceiveId)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(uriPref);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                string uri = "api/invitations?SendId=" + SendId + "&ReceiveId=" + ReceiveId;
               // Debug.Write(uri);
                HttpResponseMessage response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
        }

        public async Task<bool> PostInvitation(Invitation i)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(uriPref);

            client.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.PostAsJsonAsync("api/invitations", i);
            if (response.IsSuccessStatusCode)
            {
                // MessageBox.Show("User Added");
                return true;
            }
            else
            {
                //MessageBox.Show("Error Code" +
                //response.StatusCode + " : Message - " + response.ReasonPhrase);
                return false;
            }

        }//Post


        public async Task<User> GetUser(User user)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(uriPref);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/users?login=" + user.UserName);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<User>();
                }
                return null;
            }
        }

        public async Task<IEnumerable<User>> GetUsers(string type = "Wszyscy", int id = -1, string phrase="")
        {

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(uriPref);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                string phr = phrase == "" ? "" : "&phrase=" + phrase;
                string uri = "api/users?type=" + type + "&idUsera=" + id+phr;
                Debug.Write(uri);
                HttpResponseMessage response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<IEnumerable<User>>();
                }
                return null;
            }
        }

        public async Task<User> PostUser(User u)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(uriPref);

            client.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.PostAsJsonAsync("api/users", u);
            if (response.IsSuccessStatusCode)
            {
                // MessageBox.Show("User Added");
                return await response.Content.ReadAsAsync<User>();
            }
            else
            {
                //MessageBox.Show("Error Code" +
                //response.StatusCode + " : Message - " + response.ReasonPhrase);
                return null;
            }

        }//Post

        public async Task<IEnumerable<District>> GetDistricts()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(uriPref);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/districts");
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<IEnumerable<District>>();
                }
                MessageBox.Show("Error Code" +
                    response.StatusCode + " : Message - " + response.ReasonPhrase);
                return null;
            }
        }

        public async Task<IEnumerable<Place>> GetPlaces(string phrase = "")
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(uriPref);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                string uri = phrase == "" ? "api/places" : "api/places?phrase=" + phrase;
                Debug.Write(uri);
                HttpResponseMessage response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<IEnumerable<Place>>();
                }
                return null;
            }
        }

        public async Task<bool> PutPlace(Place p)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(uriPref);

            client.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.PutAsJsonAsync("api/places/" + p.Id, p);
            if (response.IsSuccessStatusCode)
            {
                // MessageBox.Show("User Edited");
                return true;
            }
            else
            {
                MessageBox.Show("Error Code" +
               response.StatusCode + " : Message - " + response.ReasonPhrase);
                return false;
            }
        }//Put

        public async Task<IEnumerable<Category>> GetCategories()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(uriPref);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/categories");
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<IEnumerable<Category>>();
                }
                MessageBox.Show("Error Code" +
                    response.StatusCode + " : Message - " + response.ReasonPhrase);
                return null;
            }
        }
        //api/events?category=Nauka&district=Śródmieście&period=Wszystkie&sort=Dacie
        public async Task<IEnumerable<Event>> GetEvents(string phrase = "", string category = "Wszystkie", string district = "Wszystkie", string period = "Wszystkie",
                         string typeOfEvents = "Wszystkie", int id = -1, string sort = "Dacie", int whichPage = 1)
        {
            if (phrase != "") phrase = "&phrase=" + phrase;

            if (typeOfEvents != "Wszystkie" && id < 1)//nie ma co pobierac
            {
                return new List<Event>();
            }

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(uriPref);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                string get = "api/events?category=" + category + phrase + "&district=" + district +
                    "&period=" + period + "&typeOfEvents=" + typeOfEvents + "&UserId=" + id + "&sort=" + sort + "&page=" + whichPage;
                HttpResponseMessage response = await client.GetAsync(get);
                //MessageBox.Show(get);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<IEnumerable<Event>>();
                }
                else
                {
                    MessageBox.Show("Error Code" +
                    response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
                return null;
            }
        }// GetEvents

        public async Task<bool> Put(Event e)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(uriPref);

            client.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));

            // Event e = GetEvent();

            // MessageBox.Show(e.EventName + " " + e.Category.CategoryName + "" + e.Place.PlaceName);

            var response = await client.PutAsJsonAsync("api/events/" + e.Id, e);
            if (response.IsSuccessStatusCode)
            {
                // MessageBox.Show("User Edited");
                return true;
            }
            else
            {
                MessageBox.Show("Error Code" +
               response.StatusCode + " : Message - " + response.ReasonPhrase);
                return false;
            }
        }//Put

        public async Task<int> Post(Event e)//zwraca EventId
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(uriPref);

            client.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));

            //DateTime dt = DateTime.Now;
            //dt = dt.AddYears(1);
            // Event nowy = new Event { EventName = "Nowy event!!", DateOfStart = dt, Description = "desc", CategoryId = 1, NumberOfParticipants = 12, PlaceId = 1, Price = 5 };
            var response = await client.PostAsJsonAsync("api/events", e);
            if (response.IsSuccessStatusCode)
            {
                int i = await response.Content.ReadAsAsync<int>();
                return i;
            }
            else
            {
                MessageBox.Show("Error Code" +
               response.StatusCode + " : Message - " + response.ReasonPhrase);
                return -1;
            }

        }//Post


        public async Task<bool> Delete(Event e)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(uriPref);

            client.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));

            //DateTime dt = DateTime.Now;
            //dt = dt.AddYears(1);

            // Event nowy = new Event { EventName = "Nowy event!!", DateOfStart = dt, Description = "desc", CategoryId = 1, NumberOfParticipants = 12, PlaceId = 1, Price = 5 };
            var response = await client.DeleteAsync("api/events/" + e.Id);
            if (response.IsSuccessStatusCode)
            {
                //MessageBox.Show("Event ");
                return true;
            }
            else
            {
                MessageBox.Show("Error Code" +
               response.StatusCode + " : Message - " + response.ReasonPhrase);
                return false;
            }

        }//Delete

    }//class
}//namespace
